# Python 상속: 단일, 다중, 다단계

## <a name="intro"></a> 개요
상속은 객체 지향 프로그래밍(OOP)의 주요 특징 중 하나로 기존 클래스에서 새로운 클래스를 만들 수 있다. 이렇게 함으로써 부모 클래스의 코드와 기능을 재사용하고 자식 클래스에 새로운 속성과 메서드를 추가할 수 있다.

이 포스팅에서는 다음과 같은 내용을 다룰 것이다.

- Python에서 상속이란 무엇이며 왜 유용한가
- 상속을 사용하여 상위 클래스에서 하위 클래스를 만드는 방법
- Python의 상속 유형 단일, 다중 및 다단계의 차이점 
- MRO(Method Resolution Order)를 이용하여 상속순위를 결정하는 방법
- Python에서 상속의 장점과 단점

이 포스팅의 읽기를 마치면 상속을 사용하여 Python OOP의 부모 클래스에서 하위 클래스를 만들 수 있다.

시작한다!

## <a name="sec_02"></a> Python에서 상속이란?
상속은 객체 지향 프로그래밍(OOP)에서 기존 클래스에서 새로운 클래스를 만들 수 있는 개념이다. 클래스는 클래스의 인스턴스인 객체를 만들기 위한 청사진이다. 클래스는 특성과 동작을 정의하는 속성(변수)과 메서드(함수)를 가질 수 있다.

기존 클래스에서 새로운 클래스를 생성할 때는 부모 클래스(기저 클래스 또는 슈퍼 클래스라고도 함)에서 하위 클래스(자녀 클래스 또는 파생 클래스라고도 함)를 생성하는 것이다. 하위 클래스는 부모 클래스의 모든 속성과 메서드를 상속받으며, 새로운 클래스를 추가하거나 기존 클래스를 수정할 수도 있다.

상속은 부모 클래스의 코드와 기능을 재사용할 수 있고, 여러 클래스에서 동일한 코드를 반복하지 않기 때문에 유용하다. 또한 보다 일반적이고 추상적인 클래스에서 보다 구체적이고 전문화된 클래스를 만들고, 계층적이고 논리적인 방식으로 코드를 구성할 수 있다.

예를 들어 name, age와 color 같은 속성과 eat, sleep과 make_sound 같은 메서드를 가진 Animal이라는 클래스가 있다고 가정해보자. 이러한 속성과 메서드를 상속하는 Dog, Cat, Bird 같은 하위 클래스를 Animal 클래스로부터 만들 수 있으며 새로운 클래스를 추가하거나 기존 클래스를 수정할 수도 있다. 예를 들어 Dog 클래스는 breed라는 새로운 속성과 fetch라는 새로운 메서드를 가질 수 있다. Cat 클래스는 "Meow"를 반환하는 make_sound라는 수정된 메서드를 가질 수 있다. Bird 클래스는 fly라는 새로운 메서드를 가질 수 있다.

Python에서는 하위 클래스 정의에 부모 클래스의 이름을 인수로 전달하여 상속에 사용할 수 있다. 예를 들어 `Animal`에서 상속하는 `Dog` 클래스를 만들려면 다음과 같이 작성할 수 있다.

```python
# Define the parent class
class Animal:
    # Initialize the attributes
    def __init__(self, name, age, color):
        self.name = name
        self.age = age
        self.color = color
    
    # Define the methods
    def eat(self, food):
        print(self.name + " is eating " + food)
    
    def sleep(self):
        print(self.name + " is sleeping")
    
    def make_sound(self):
        print(self.name + " is making a sound")
# Define the subclass
class Dog(Animal): # Pass the parent class as an argument
    # Initialize the attributes
    def __init__(self, name, age, color, breed):
        # Call the parent class constructor
        super().__init__(name, age, color)
        # Add a new attribute
        self.breed = breed
    
    # Define a new method
    def fetch(self, item):
        print(self.name + " is fetching " + item)
```

이제 `Dog` 클래스의 객체를 만들고 해당 객체의 속성과 메서드 및 `Animal` 클래스에서 상속된 객체를 사용할 수 있다. 예를 들면 다음과 같다.

```python
# Create a Dog object
dog = Dog("Max", 3, "brown", "Labrador")
# Use the inherited attributes and methods
print(dog.name) # Max
print(dog.age) # 3
print(dog.color) # brown
dog.eat("bone") # Max is eating bone
dog.sleep() # Max is sleeping
dog.make_sound() # Max is making a sound
# Use the new attribute and method
print(dog.breed) # Labrador
dog.fetch("ball") # Max is fetching ball
```

하나의 부모 클래스로부터 서브 클래스에 상속되는 단일 상속의 예이다. 그러나 Python은 서브 클래스가 하나 이상의 부모 클래스로부터 상속받을 수 있는 다중 상속과 다른 서브 클래스로부터 상속받을 수 있는 다단계 상속도 지원한다. 이러한 상속 유형에 대해서는 [다음 절](#sec_03)에서 살펴볼 것이다.

## <a name="sec_03"></a> Python의 상속 유형
[앞 절](#sec_02)에서 Python에서 상속이 무엇인지, 그리고 그것을 사용하여 부모 클래스로부터 하위 클래스를 만드는 방법을 설명하였다. 이 절에서는 Python에서 상속의 다양한 유형과 그것들이 하위 클래스의 속성과 메서드에 어떤 영향을 미치는지 탐구할 것이다.

Python의 상속은 크게 단일, 다중, 다단계의 세 가지가 있다. 그들이 무엇을 의미하고 어떻게 작동하는지 살펴보자.

### 단일 상속:
단일 상속은 가장 간단한 유형으로 클래스가 한 부모 클래스로부터만 속성과 메서드를 상속받는 것이다. 파생 클래스(하위 클래스라고도 함)는 새로운 속성이나 메서드를 추가하거나 기존의 속성을 재정의함으로써 부모 클래스(기저 클래스라고도 함)의 기능을 확장한다.

### 다중 상속:
다중 상속은 하나의 클래스가 둘 이상의 부모 클래스로부터 속성과 메소드를 상속받을 수 있게 한다. 이는 파생된 클래스가 여러 개의 부모 클래스를 가질 수 있으며, 그 속성과 메소드를 모두 상속받는다는 것을 의미한다. 이는 둘 이상의 부모 클래스가 동일한 속성 또는 메소드를 정의할 경우 모호성이 발생하는 다이아몬드 문제로 이어질 수 있다.

### 다단계 상속:
다단계 상속은 두 가지 이상의 레벨을 갖는 상속의 연쇄(chain)를 생성하는 것을 포함한다. 이는 파생된 클래스가 한 클래스로부터 상속받을 수 있고, 그 자체가 다른 클래스로부터 상속받아 계층 구조를 형성할 수 있음을 의미한다. 각 하위 클래스는 최상위 기저 클래스부터 자신의 직계 부모 클래스까지 속성과 메소드를 상속받는다.

이러한 상속 유형은 Python에서 코드를 구성하고 재사용하는 데 유연성을 제공하므로 개발자는 복잡한 클래스 계층 구조를 만들고 코드 재사용과 모듈화를 촉진할 수 있다. 그러나 코드 명확성을 유지하고 모호성이나 긴밀한 결합과 같은 잠재적 문제를 방지하기 위해 상속을 신중하게 사용할 필요가 있다.

## <a name="sec_04"></a> Python에서 단일 상속
이 절에서는 Python에서의 단일 상속에 대해 좀 더 자세히 살펴볼 것이다. 부모 클래스로부터 상속된 속성과 메서드에 접근하여 수정하는 방법과 하위 클래스로 상속된 속성을 재정의하는 방법을 살펴볼 것이다. 클래스와 객체 간의 상속 관계를 확인하기 위해 내장된 함수 `isinstance()`와 `isubclass()`를 사용하는 방법도 살펴볼 것이다.

### 상속된 속성 및 메서드 액세스 및 수정
하위 클래스가 상위 클래스로부터 상속받을 때, 하위 클래스는 상위 클래스의 모든 속성과 메소드를 상속받는다. 그러나 하위 클래스는 이러한 상속된 속성과 메소드에 서로 다른 방식으로 접근하고 수정할 수도 있다.

한 가지 방법은 상위 클래스에 대한 참조를 반환하는 `super()` 함수를 사용하는 것이다. `super()` 함수는 하위 클래스가 부모 클래스의 속성과 메서드에 명시적으로 이름을 붙이지 않고 접근할 수 있도록 한다. 예를 들어, [앞 절](#sec_03)에서 정의한 `Dog` 클래스에서 `super()._init_(name, age, color)`를 사용하여 `Animal` 클래스의 생성자를 호출하고 상속된 속성을 초기화했다.

```python
# Define the subclass
class Dog(Animal): # Pass the parent class as an argument
    # Initialize the attributes
    def __init__(self, name, age, color, breed):
        # Call the parent class constructor
        super().__init__(name, age, color) # Use super() to access the parent class
        # Add a new attribute
        self.breed = breed
```

또 다른 방법은 부모 클래스의 이름에 점(`.`), 속성 또는 메소드 이름을 사용하는 것이다. 이는 하위 클래스에 의해 재정의되지 않는 부모 클래스의 특정 속성 또는 메소드를 액세스하고자 할 때 유용하다. 예를 들어 `Dog` 클래스에서 `Animal` 클래스의 `make_sound` 메소드를 액세스하고 싶다고 가정하면 다음과 같이 작성할 수 있다.

```python
# Define the subclass
class Dog(Animal): # Pass the parent class as an argument
    # Initialize the attributes
    def __init__(self, name, age, color, breed):
        # Call the parent class constructor
        super().__init__(name, age, color)
        # Add a new attribute
        self.breed = breed
    
    # Define a new method
    def fetch(self, item):
        print(self.name + " is fetching " + item)
    
    # Access the parent class method
    def make_animal_sound(self):
        Animal.make_sound(self) # Use the parent class name to access the parent class method
```

세 번째 방법은 서브클래스의 이름을 사용하고 점(`.`)과 속성 또는 메소드 이름을 사용하는 것이다. 이는 서브클래스 자체에서 상속된 속성 또는 메소드에 접근하거나 수정하고자 할 때 유용하다. 예를 들어, `Dog` 클래스에서 `Animal` 클래스의 `make_sound` 메소드를 수정하고자 할 때, 다음과 같이 작성할 수 있다.

```python
# Define the subclass
class Dog(Animal): # Pass the parent class as an argument
    # Initialize the attributes
    def __init__(self, name, age, color, breed):
        # Call the parent class constructor
        super().__init__(name, age, color)
        # Add a new attribute
        self.breed = breed
    
    # Define a new method
    def fetch(self, item):
        print(self.name + " is fetching " + item)
    
    # Modify the inherited method
    def make_sound(self):
        print(self.name + " is barking") # Use the subclass name to modify the inherited method
```

## <a name="sec_05"></a> Python의 다중 상속
다중 상속은 Python에서 서브클래스가 둘 이상의 부모 클래스로부터 상속받을 수 있도록 하는 상속의 한 종류이다. 이는 서브클래스가 여러 부모 클래스의 속성과 메소드에 접근하여 사용할 수 있고, 다양한 방식으로 결합할 수 있음을 의미한다.

다중 상속은 서로 다른 상위 클래스의 기능을 가진 하위 클래스를 만들고 불필요한 중간 클래스를 만드는 것을 방지하는 데 유용하다. 예를 들어 속성과 메서드가 다른 두 개의 상위 클래스인 Employee와 Student가 있다고 가정합니다. Employee와 Student 모두에서 상속되고 두 클래스의 속성과 메서드가 있는 하위 클래스인 WorkingStudent를 만들 수 있다.

Python에서 다중 상속을 구현하려면 상위 클래스의 이름을 쉼표로 구분하여 하위 클래스 정의에 인수로 전달하기만 하면 된다. 예를 들어 다음과 같다.

```python
# Define the first parent class
class Employee:
    # Initialize the attributes
    def __init__(self, name, salary):
        self.name = name
        self.salary = salary
    
    # Define a method
    def work(self):
        print(self.name + " is working")
# Define the second parent class
class Student:
    # Initialize the attributes
    def __init__(self, name, school):
        self.name = name
        self.school = school
    
    # Define a method
    def study(self):
        print(self.name + " is studying")
# Define the subclass
class WorkingStudent(Employee, Student): # Pass the parent classes as arguments
    # Initialize the attributes
    def __init__(self, name, salary, school):
        # Call the parent class constructors
        Employee.__init__(self, name, salary)
        Student.__init__(self, name, school)
```

이제 `WorkingStudent` 클래스의 객체를 만들고 객체의 속성과 메서드는 Employee 및 Student 클래스에서 상속된 객체를 사용할 수 있다. 예를 들면 다음과 같다.

```python
# Create a WorkingStudent object
ws = WorkingStudent("Alice", 5000, "MIT")
# Use the inherited attributes and methods
print(ws.name) # Alice
print(ws.salary) # 5000
print(ws.school) # MIT
ws.work() # Alice is working
ws.study() # Alice is studying
```

다중 상속을 통해 여러 부모 클래스에서 더 복잡하고 다용도로 사용할 수 있는 클래스를 만들 수 있다. 그러나 다이아몬드 문제와 방법 해결 순서(MRO)와 같은 몇 가지 과제와 복잡성도 소개한다. 이러한 문제에 대해서는 [다음 절](#seb_07)에서 논의할 것이다.

## <a name="sec_06"></a> Python의 다단계 상속
다단계 상속은 Python의 상속의 한 종류로, 서브클래스가 다른 서브클래스로부터 상속을 받을 수 있도록 하고, 이를 다시 부모 클래스로부터 상속받는다. 이는 서브클래스가 여러 클래스의 속성과 메소드를 계층적 순서로 접근하여 사용할 수 있음을 의미한다.

다단계 상속은 여러 클래스의 특징을 가진 하위 클래스를 만들고 코드의 논리적이고 일관된 구조를 유지하려는 경우에 유용하다. 예를 들어, Animal, Dog 및 Labrador의 세 클래스가 있다고 가정해 보겠다. 여기서 Dog는 Animal의 하위 클래스이고 Labrador는 Dog의 하위 클래스이다. Labrador에서 상속받아 하위 클래스인 LabradorPuppy를 만들 수 있으며, 하위 클래스인 LabradorPuppy는 세 클래스 Animal, Dog와 Labrador 세 클래스의 속성과 메서드를 모두 갖는다. 

Python에서 다단계 상속을 구현하기 위해서는 바로 부모 클래스의 이름을 서브클래스 정의에 인수로 전달하기만 하면 된다. 예를 들면 다음과 같다.

```python
# Define the first parent class
class Animal:
    # Initialize the attributes
    def __init__(self, name, age, color):
        self.name = name
        self.age = age
        self.color = color
    
    # Define a method
    def eat(self, food):
        print(self.name + " is eating " + food)
# Define the second parent class
class Dog(Animal): # Pass the first parent class as an argument
    # Initialize the attributes
    def __init__(self, name, age, color, breed):
        # Call the first parent class constructor
        super().__init__(name, age, color)
        # Add a new attribute
        self.breed = breed
    
    # Define a method
    def bark(self):
        print(self.name + " is barking")
# Define the third parent class
class Labrador(Dog): # Pass the second parent class as an argument
    # Initialize the attributes
    def __init__(self, name, age, color, breed, size):
        # Call the second parent class constructor
        super().__init__(name, age, color, breed)
        # Add a new attribute
        self.size = size
    
    # Define a method
    def fetch(self, item):
        print(self.name + " is fetching " + item)
# Define the subclass
class LabradorPuppy(Labrador): # Pass the third parent class as an argument
    # Initialize the attributes
    def __init__(self, name, age, color, breed, size, owner):
        # Call the third parent class constructor
        super().__init__(name, age, color, breed, size)
        # Add a new attribute
        self.owner = owner
    
    # Define a method
    def play(self):
        print(self.name + " is playing with " + self.owner)
```

이제, `LabradorPuppy` 클래스의 객체를 만들고 그 속성과 메서드로, 그리고 `Animal`, `Dog`,`Labrador` 클래스에서 물려받은 속성과 방법을 사용할 수 있다. 예를 들면 다음과 같다.

```python
# Create a LabradorPuppy object
lp = LabradorPuppy("Buddy", 1, "yellow", "Labrador", "large", "Bob")
# Use the inherited attributes and methods
print(lp.name) # Buddy
print(lp.age) # 1
print(lp.color) # yellow
print(lp.breed) # Labrador
print(lp.size) # large
lp.eat("kibble") # Buddy is eating kibble
lp.bark() # Buddy is barking
lp.fetch("stick") # Buddy is fetching stick
# Use the new attribute and method
print(lp.owner) # Bob
lp.play() # Buddy is playing with Bob
```

다단계 상속은 좀 더 일반적이고 추상적인 것에서 좀 더 구체적이고 전문화된 클래스를 만들 수 있도록 해주며, 여러 클래스의 특징을 계층적 순서로 상속받을 수 있다. 다만, 상속의 순서와 방법 해결 순서(MRO)에 대해서도 주의를 기울일 필요가 있으며, 이에 대해서는 [다음 절](#sec_07)에서 논의할 것이다.

## <a name="sec_07"></a> Python에서의 MRO(Method Resolution Order)
본 절에서는 Python 클래스의 계층 구조에서 Python이 메소드나 속성을 찾는 순서인 MRO(Method Resolution Order)에 대해 논의하고자 한다. MRO는 Python이 서로 다른 부모 클래스 간의 충돌과 모호성을 해결하는 방법을 결정하기 때문에 다중 상속 또는 다단계 상속을 사용할 때 이를 이해하는 것이 매우 중요하다.

### MRO는 무엇이며 Python은 어떻게 해결할까?
MRO는 Python이 객체에 대한 메소드나 속성을 찾으려고 할 때 검색하는 클래스의 시퀀스이다. 예를 들어, 객체에 메소드를 호출하면 Python은 먼저 그 객체의 클래스에서 메소드를 찾고, 그 부모 클래스에서 찾고, 그 부모 클래스에서 찾고, 그 다음 조부모 클래스에서 찾고 셰층 구조의 끝에 도달할 때까지 찾는다.

MRO는 서브클래스 정의의 상속 순서와 C3 선형화 알고리즘이라는 특별한 알고리즘에 의해 결정된다. C3 알고리즘은 MRO가 다음 세 가지 원칙을 따르도록 보장한다.

- 하위 클래스는 부모 클래스들보다 우선헌다.
- 부모 클래스들은 하위 클래스 정의의 상속 순서에 따라 순서가 지정된다.
- 클래스가 여러 상위 클래스로부터 상속되는 경우 클래스의 MRO는 상위 클래스의 MRO와 클래스 자체의 조합이다.

예를 들어, 다음과 같은 클래스가 있다고 가정해 보겠다.

```python
# Define the first parent class
class A:
    def method(self):
        print("Method of class A")
# Define the second parent class
class B:
    def method(self):
        print("Method of class B")
# Define the subclass
class C(A, B): # Pass the parent classes as arguments
    pass
```

클래스 `C`의 MRO는 [`C`, `A`, `B`, `object`]이며, 여기서 `object`는 Python의 모든 클래스의 기저 클래스이다. 이때 클래스 C의 객체에서 `method`가 호출되면, Python은 먼저 클래스 C에서 `method`를 찾고, 다음 클래스 `A`에서 찾고, 다음 클래스 `B`에서 찾고, 마지막으로 `object`에서 `method`를 찾을 것이라는 것을 의미한다. 클래스 `C`에는 `method`가 없으므로, Python은 클래스 `A`에서 메소드를 찾아서 실행할 것이다. 예를 들면 다음과 같다.

```python
# Create an object of class C
c = C()
# Call the method
c.method() # Method of class A
```

우리가 하위 클래스 정의에서 상속 순서를 바꾸면 그에 따라 MRO도 달라질 것이다. 예를 들어 클래스 `C`를 클래스 `C(B, A)`로 정의하면 MRO는 [`C`, `B`, `A`, `object`]가 될 것이고 Python은 클래스 `A` 대신 클래스 `B`에서 `method`를 찾을 것이다. 예를 들면 다음과 같다.

```python
# Define the subclass
class C(B, A): # Change the order of inheritance
    pass
# Create an object of class C
c = C()
# Call the method
c.method() # Method of class B
```

내장 함수 `mro()`를 사용하여 모든 클래스의 MRO를 볼 수 있다. 예를 들면 다음과 같다.

```python
# See the MRO of class C
print(C.mro()) # [`C`, `B`, `A`, `object`]
```

## <a name="sec_08"></a> Python에서의 상속의 장점과 단점
본 절에서는 Python에서 상속의 장단점과 이를 효과적으로 사용하기 위한 몇 가지 모범 사례 및 팁에 대해 논의할 것이다.

### Python에서 상속의 장점
상속은 객체 지향 프로그래밍(OOP)의 강력한 기능으로 다음과 같은 많은 이점을 제공한다:

- **코드 재사용**: 상속을 통해 상위 클래스의 코드와 기능을 재사용할 수 있어 여러 클래스에서 동일한 코드를 반복하지 않는다. 이를 통해 코드가 더 간결하고 일관성 있으며 유지 관리하기 쉽다.
- **다형성**: 상속은 부모 클래스와 다른 동작과 특징을 가진 하위 클래스를 만들고 상속된 속성과 메소드를 무시하거나 수정할 수 있다. 이를 통해 코드는 유연하고 역동적이며 다양한 상황에 적응할 수 있다.
- **추상화**: 상속을 통해 클래스 그룹의 공통 속성과 메서드와 그들로부터 상속되는 보다 구체적이고 전문적인 클래스를 정의하는 보다 일반적이고 추상적인 클래스를 만들 수 있다. 이를 통해 코드를 보다 체계적이고 논리적이며 모듈러하게 구성할 수 있다.

### Python에서 상속의 단점
그러나 상속에는 다음과 같은 몇 가지 단점과 과제도 있다.

- **복잡성**: 상속은 코드를 더욱 복잡하고 이해하기 어렵게 만들 수 있으며, 특히 다중 상속 또는 다단계 상속을 사용할 때 서로 다른 부모 계층 간의 충돌과 모호성을 처리해야 한다.
- **커플링**: 상속은 부모 클래스와 하위 클래스 사이에 강한 의존성과 커플링을 만들 수 있으며, 이는 부모 클래스의 변경이 하위 클래스에 영향을 미칠 수 있음을 의미하며, 그 반대의 경우도 마찬가지이다. 이것은 코드를 덜 유연하게 만들고 오류가 발생하기 쉽게 만들 수 있다.
- **남용**: 상속이 남용되고 오용되어 논리적으로 관련이 없거나 일관성이 없는 클래스의 불필요하고 과도한 계층 구조를 초래할 수 있다. 이는 코드의 가족성을 떨어뜨리고 유지 관리가 어렵게 만들 수 있다.

### Python에서 상속을 사용하는 모범 사례 및 팁
상속을 효과적으로 사용하고 단점을 방지하기 위해 다음과 같은 몇 가지 모범 사례와 팀을 보이고자 한다.

- 상속은 여러분의 코드에 합당하고 가치를 더할 때에만 사용하라. 상속을 사용하기 위해, 또는 상속 없이도 할 수 있는 것을 성취하기 위해 사용하지 말라.
- 상속을 사용하여 클래스 간의 "is-a" 관계를 모델링하는 것이 아니라 "has-a" 관계를 모델링한다. 예를 들어, 개는 동물이므로 상속을 사용하는 것이 타당하다. 그러나 자동차에는 엔진이 있으므로 상속을 사용하는 것이 타당하지 않다. 대신 한 클래스를 다른 클래스의 속성으로 사용하여 클래스를 결합하는 또 다른 방법인 합성(composition)을 사용할 수 있다.
- 가능한 한 단일 상속을 사용하고, 반드시 필요한 경우가 아니면 다중 상속을 피하십시오. 다중 상속은 코드에 더 많은 복잡성과 혼란을 초래할 수 있으므로 주의를 기울여 사용해야 한다.
- MRO(Method Resolution Order)를 사용하여 하위 클래스의 상속 순서와 행동을 이해하고 통제할 수 있다. 내장 함수 `mro()`를 사용하여 모든 클래스의 MRO를 볼 수 있고, `super()` 함수를 사용하여 상위 클래스 속성과 메소드에 접근할 수 있다.
- 추상적 기저 클래스(`ABC`)를 사용하여 상속 대상 클래스이지만 인스턴스화되지 않는 클래스를 만들 수 있다. ABC는 적어도 하나의 추상적 메소드를 갖는 클래스로, 구현이 없고 하위 클래스에 의해 재정의되어야 하는 메소드이다. Python에서 `abc` 모듈을 사용하여 ABC를 생성하고 사용할 수 있다.

상속은 Python OOP의 유용하고 강력한 기능으로 기존 클래스에서 보다 복잡하고 다재다능한 클래스를 만들 수 있다. 하지만 주의해서 다루어야 할 몇 가지 단점과 과제도 있다. 이 절에서 설명한 모범 사례와 팁을 따르면 상속을 효과적으로 사용할 수 있으며 상속의 함정을 피할 수 있다.

이것으로 Python 상속에 대한 포스팅을 마친다.

## <a name="summary"></a> 요약
이 포스팅에서는 기존 클래스에서 새로운 클래스를 생성할 수 있는 객체 지향 프로그래밍(OOP)의 특징인 상속에 대해 알아봤다. 여기서 다음 주제를 다루었다.

- Python에서 상속이란 무엇이며 왜 유용한가
- 상속을 사용하여 상위 클래스로부터 하위 클래스를 만드는 방법
- Python의 상속 유형인 단일, 다중 및 다단계는 무엇인가?
- MRO(Method Resolution Order)를 이용하여 상속 순위를 결정하는 방법
- Python에서 상속의 장점과 단점은 무엇일까?
- 상속을 효과적으로 사용하기 위한 몇 가지 모범 사례 및 팁

상속은 기존 클래스에서 더 복잡하고 모듈화된 클래스를 만들고 상위 클래스의 코드와 기능을 재사용하는 데 도움을 줄 수 있는 유용하고 중요한 개념이다. 그러나 상속에는 상속의 복잡성, 결합 및 남용과 같은 몇 가지 단점과 신중하게 처리해야 하는 과제도 있다.

이 포스팅에서 설명한 모범 사례와 팁을 따르면 상속을 효과적으로 사용하고 상속의 함정을 피할 수 있다. 또한 내장된 함수 `mro()`와 `super()`를 사용하여 상속 순서와 하위 클래스의 동작을 확인하고 제어할 수 있다. `abc` 모듈을 사용하여 상속 대상이지만 인스턴스화되지 않은 추상적 기저 클래스(ABC)를 생성하고 사용할 수도 있다.

상속은 Python OOP의 강력하고 유용한 기능으로 기존 클래스에 보다 복잡하고 다재다능한 수업을 만드는 데 도움을 줄 수 있다. 그러나 Python에서 클래스를 결합하고 연관시키는 유일한 방법은 아니다. 논리적이고 일관된 방식으로 클래스를 만들고 구성하는 데 도움을 줄 수 있는 구성과 집계와 같은 다른 방법도 있다. 이러한 개념은 다른 곳에서 살펴볼 것이다.
