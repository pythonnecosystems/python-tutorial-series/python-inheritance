# Python 상속: 단일, 다중, 다단계 <sup>[1](#footnote_1)</sup>

> <font size="3">Python OOP에서 클래스와 객체를 생성하고 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./inheritance.md#intro)
1. [Python에서 상속이란?](./inheritance.md#sec_02)
1. [Python의 상속 유형](./inheritance.md#sec_03)
1. [Python에서 단일 상속](./inheritance.md#sec_04)
1. [Python의 다중 상속](./inheritance.md#sec_05)
1. [Python의 다단계 상속](./inheritance.md#sec_06)
1. [Python에서의 MRO(Method Resolution Order)](./inheritance.md#sec_07)
1. [Python에서의 상속의 장점과 단점](./inheritance.md#sec_08)
1. [요약](./inheritance.md#summary)


<a name="footnote_1">1</a>: [Python Tutorial 21 — Python Inheritance: Single, Multiple, Multilevel](https://python.plainenglish.io/python-tutorial-21-python-inheritance-single-multiple-multilevel-5eca0f4ae257?sk=b0ba122156102448f67d47e79b22659b)를 편역하였다.
